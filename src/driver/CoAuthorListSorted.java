/**
 * @author Osama This class extracts the names of the co-authors and orders them
 */
package driver;

import java.util.ArrayList;
import java.util.Collections;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CoAuthorListSorted {

  /**
   * extracts the names of the co-authors and orders them
   * 
   * @param inputFiles
   * @return co-author names ordered
   */
  public String execute(String[] inputFiles) {
    // TODO Auto-generated method stub
    ArrayList b = new ArrayList();
    String result = "";
    for (String inputFile : inputFiles) {
      try {
        HTML htmlParser = new HTML();
        String rawHTMLString = htmlParser.getHTML(inputFile);

        String coAuthorExtract =
            "<a class=\"cit-dark-link\" href=(.*?)" + " title=(.*?)>(.*?)</a>";
        Pattern patternObject = Pattern.compile(coAuthorExtract);
        Matcher matcherObject = patternObject.matcher(rawHTMLString);
        while (matcherObject.find()) {
          b.add(matcherObject.group(3));
        }
        Collections.sort(b);
        b.remove("Citations");
        result = ("7. Co-Author list sorted (Total: " + b.size() + ")");
        for (int i = 0; i < b.size(); i++) {
          result = result + "\n" + (b.get(i));
        }

      } catch (Exception e) {
        return ("malformed URL or cannot open connection to given URL");
      }
    }
    return result;
  }
}
