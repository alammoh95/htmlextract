/**
 * @author Osama This class extracts the number of citations of an author
 * 
 */
package driver;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AllCitations implements Extractor {

  /**
   * Extracts the number of citations of an author
   */
  public String Extract(String inputFile) {
    String result = "";
    try {
      HTML htmlParser = new HTML();
      String rawHTMLString = htmlParser.getHTML(inputFile);
      String citationExtract =
          "<td class=\"cit-borderleft cit-data\">(.*?)</td>";
      Pattern patternObject = Pattern.compile(citationExtract);
      Matcher matcherObject = patternObject.matcher(rawHTMLString);
      if (matcherObject.find()) {
        result =
            ("2. Number of All Citations: " + "\n" + ("      " + matcherObject
                .group(1)));
      }

    } catch (Exception e) {
      return ("malformed URL or cannot open connection to given URL");
    }
    return result;

  }

}
