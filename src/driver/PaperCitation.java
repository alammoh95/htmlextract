/**
 * @author Osama This class retrieves the first 5 papers' number of citations
 */
package driver;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PaperCitation implements Extractor {

  /**
   * retrieves the first 5 papers' number of citations
   */
  public String Extract(String inputFile) {
    String result = "5. Total paper citation (first 5 papers):";
    try {
      HTML htmlParser = new HTML();
      String rawHTMLString = htmlParser.getHTML(inputFile);

      String paperCitationExtract =
          "<a class=\"cit-dark-link\"" + " href=(.*?)>([0-9]*)</a>";
      Pattern patternObject = Pattern.compile(paperCitationExtract);
      Matcher matcherObject = patternObject.matcher(rawHTMLString);
      int counter = 0;
      int citations = 0;
      while (matcherObject.find()) {
        if (counter == 0)
          counter++;
        else if (counter < 6) {
          citations = citations + Integer.parseInt(matcherObject.group(2));
          counter++;
        } else
          break;
      }
      result = result + ("\n" + "          " + citations);

    } catch (Exception e) {
      return ("malformed URL or cannot open connection to given URL");
    }
    return result;
  }
}
