/**
 * @author Osama
 * This class retrieves the title of the first 3 publications
 */
package driver;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Publications implements Extractor {

  /**
   * retrieves the title of the first 3 publications
   */
  public String Extract(String inputFile) {
    // TODO Auto-generated method stub
    String result = "4. Title of the first 3 publications: ";
    try {
      HTML htmlParser = new HTML();
      String rawHTMLString = htmlParser.getHTML(inputFile);

      String publicationExtract =
          "<a href=(.*?)" + " class=\"cit-dark-large-link\">(.*?)</a>";
      Pattern patternObject = Pattern.compile(publicationExtract);
      Matcher matcherObject = patternObject.matcher(rawHTMLString);
      // System.out.println("4. Title of the first 3 publications: ");
      int counter = 1;
      while (matcherObject.find()) {
        result =
            result
                + ("\n" + "      " + counter + "- " + matcherObject.group(2));
        counter++;
        if (counter == 4)
          break;
      }

    } catch (Exception e) {
      return ("malformed URL or cannot open connection to given URL");
    }
    return result;

  }

}
