/**
 * @author Osama This class extracts the name of the author
 */
package driver;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Author implements Extractor {

  /**
   * extracts the name of the author
   */
  public String Extract(String inputFile) {
    // TODO Auto-generated method stub
    String result = "4";
    try {
      HTML htmlParser = new HTML();
      String rawHTMLString = htmlParser.getHTML(inputFile);

      String reForAuthorExtraction =
          "<span id=\"cit-name-display\" "
              + "class=\"cit-in-place-nohover\">(.*?)</span>";
      Pattern patternObject = Pattern.compile(reForAuthorExtraction);
      Matcher matcherObject = patternObject.matcher(rawHTMLString);
      while (matcherObject.find()) {
        result =
            ("1. Name of Author: " + "\n" + "      " + matcherObject.group(1));
      }

    }
    // Output Error
    catch (Exception e) {
      return ("malformed URL or cannot open connection to given URL");
    }
    return result;
  }

}
