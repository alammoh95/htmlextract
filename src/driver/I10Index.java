/**
 * @author Osama This class retrieves the I10Index of the author
 */
package driver;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class I10Index implements Extractor {

  /**
   * retrieves the I10Index of the author
   */
  public String Extract(String inputFile) {
    // TODO Auto-generated method stub
    String result = ("3.  Number of i10-index after 2009: ");
    try {
      HTML htmlParser = new HTML();
      String rawHTMLString = htmlParser.getHTML(inputFile);
      String i10Extract = "<td class=\"cit-borderleft cit-data\">(.*?)</td>";
      Pattern patternObject = Pattern.compile(i10Extract);
      Matcher matcherObject = patternObject.matcher(rawHTMLString);
      int counter = 0;
      while (matcherObject.find()) {
        if (counter == 5) {
          result = result + ("\n" + "      " + matcherObject.group(1));
          break;
        } else
          counter++;
      }

    } catch (Exception e) {
      return ("malformed URL or cannot open connection to given URL");
    }
    return result;
  }

}
