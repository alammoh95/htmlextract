/**
 * @author Osama
 * This class retrieves the total number of co authors
 */
package driver;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TotalCoAuthors implements Extractor {

  @Override
  /**
   * retrieves the total number of co authors
   */
  public String Extract(String inputFile) {
    // TODO Auto-generated method stub
    String result = "6. Total Co-Authors:";
    try {
      HTML htmlParser = new HTML();
      String rawHTMLString = htmlParser.getHTML(inputFile);

      String coAuthorExtract =
          "<a class=\"cit-dark-link\" href=(.*?)" + " title=(.*?)>(.*?)</a>";
      Pattern patternObject = Pattern.compile(coAuthorExtract);
      Matcher matcherObject = patternObject.matcher(rawHTMLString);
      ArrayList b = new ArrayList();
      int citations = 0;
      while (matcherObject.find()) {
        b.add(matcherObject.group(3));
        b.remove("Citations");
      }
      result = result + ("\n" + "          " + b.size());

    } catch (Exception e) {
      return ("malformed URL or cannot open connection to given URL");
    }
    return result;
  }

}
