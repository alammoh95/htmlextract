package driver;

// **********************************************************
// Assignment3:
// UTORID user_name: alammoh5
//
// Author: Mohammad Osama Alam
//
//
// Honor Code: I pledge that this program represents my own
// program code and that I have coded on my own. I received
// help from no one in designing and debugging my program.
// *********************************************************
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MyParser {

  /**
   * @param args
   * @throws FileNotFoundException
   */
  public static void main(String[] args) throws FileNotFoundException {
    String inputFiles[] = args[0].split(",");
    Author a = new Author();
    AllCitations b = new AllCitations();
    I10Index c = new I10Index();
    Publications d = new Publications();
    PaperCitation e = new PaperCitation();
    TotalCoAuthors f = new TotalCoAuthors();
    Extractor extractors[] = {a, b, c, d, e, f};
    String output = "";
    // Print out every piece of information from each file given
    for (String inputFile : inputFiles) {
      output =
          output
              + ("-------------------------------"
                  + "---------------------------------" + "\n");
      for (Extractor x : extractors) {
        output = output + x.Extract(inputFile) + "\n";
      }
    }
    // Print out CoAuthor List sorted
    CoAuthorListSorted g = new CoAuthorListSorted();
    output =
        output + "-------------------------------"
            + "---------------------------------" + "\n"
            + g.execute(inputFiles);
    if (args.length == 2) {
      PrintWriter pw = new PrintWriter(args[1]);
      pw.print(output);
      pw.close();
    } else
      System.out.println(output);
  }
}
