package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import driver.TotalCoAuthors;

public class TotalCoAuthorsTest {
  TotalCoAuthors a;

  @Before
  public void setUp() throws Exception {
    a = new TotalCoAuthors();
  }

  @Test
  public void testExtract() {
    // Valid file and correct output 1
    String result = a.Extract("sample1.html");
    assertEquals("6. Total Co-Authors:" + "\n" + "          15", result);
    // Valid file and correct output 2
    result = a.Extract("sample2.html");
    assertEquals("6. Total Co-Authors:" + "\n" + "          14", result);
    // File Does not exist
    result = a.Extract("FileDoesn'tExist.html");
    assertEquals("malformed URL or cannot open connection to given URL",
        result);
    // Valid file and incorrect output 1
    result = a.Extract("sample1.html");
    assertNotEquals("this is clearly wrong", result);
    // Valid file and incorrect output 2
    result = a.Extract("sample2.html");
    assertNotEquals("i bet this is wrong", result);
  }

}
