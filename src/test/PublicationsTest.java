package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import driver.Publications;

public class PublicationsTest {
  Publications a;

  @Before
  public void setUp() throws Exception {
    a = new Publications();
  }

  @Test
  public void testExtract() {
    // Valid file and correct output 1
    String result = a.Extract("sample1.html");
    assertEquals("4. Title of the first 3 publications: " + "\n"
        + "      1- Bioclipse: an open source workbench for chemo-and"
        + " bioinformatics" + "\n" + "      2- The LCB data warehouse" + "\n"
        + "      3- XMPP for cloud computing in bioinformatics supporting"
        + " discovery and invocation of asynchronous web services", result);
    // Valid file and correct output 2
    result = a.Extract("sample2.html");
    assertEquals("4. Title of the first 3 publications: " + "\n"
        + "      1- Face-tracking as an augmented input in video games:"
        + " enhancing presence, role-playing and control" + "\n"
        + "      2- Art of defense: a collaborative handheld augmented"
        + " reality board game" + "\n"
        + "      3- Sociable killers: understanding"
        + " social relationships in an online first-person shooter game",
        result);
    // File Does not exist
    result = a.Extract("FileDoesn'tExist.html");
    assertEquals("malformed URL or cannot open connection to given URL",
        result);
    // Valid file and incorrect output 1
    result = a.Extract("sample1.html");
    assertNotEquals("this is clearly wrong", result);
    // Valid file and incorrect output 2
    result = a.Extract("sample2.html");
    assertNotEquals("i bet this is wrong", result);
  }

}
