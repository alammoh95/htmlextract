package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import driver.CoAuthorListSorted;

public class CoAuthorListSortedTest {
  CoAuthorListSorted a;

  @Before
  public void setUp() throws Exception {
    a = new CoAuthorListSorted();
  }

  @Test
  public void testExecute() {
    String input[] = {"sample1.html", "sample2.html"};
    // Valid files and correct input
    String result = a.execute(input);
    assertEquals("7. Co-Author list sorted (Total: 29)" + "\n"
        + "Abigail Sellen" + "\n" + "Adam Ameur" + "\n" + "Andrew D Miller"
        + "\n" + "Antony John Williams" + "\n" + "Blair MacIntyre" + "\n"
        + "Christoph Steinbeck" + "\n" + "Deepak Jagdish" + "\n"
        + "E.D. Mynatt" + "\n" + "Egon Willighagen" + "\n"
        + "Elsa Eiriksdottir" + "\n" + "Erika Shehan Poole" + "\n"
        + "Greg Turk" + "\n" + "Iulian Radu" + "\n" + "Janna Hastings" + "\n"
        + "John Stasko" + "\n" + "Jonathan Alvarsson" + "\n" + "Komorowski Jan"
        + "\n" + "Kurt Luther" + "\n" + "Nina Jeliazkova" + "\n"
        + "Noel M. O'Boyle" + "\n" + "Rajarshi Guha" + "\n" + "Sam Adams"
        + "\n" + "Samuel Lampa" + "\n" + "Sean Ekins" + "\n" + "Thore Graepel"
        + "\n" + "Valentin Georgiev" + "\n" + "Xiang Cao" + "\n"
        + "Youn-ah Kang" + "\n" + "gilleain torrance", result);
    // File Does not exist
    String wrongInput[] = {"FileDoesn'tExist.html"};
    result = a.execute(wrongInput);
    assertEquals("malformed URL or cannot open connection to given URL",
        result);
    // Valid file and incorrect output 1
    result = a.execute(input);
    assertNotEquals("this is clearly wrong", result);
    // Valid file and incorrect output 2
    result = a.execute(input);
    assertNotEquals("i bet this is wrong", result);
  }

}
