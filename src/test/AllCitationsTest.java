package test;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import driver.AllCitations;

public class AllCitationsTest {
  AllCitations a;

  @Before
  public void setUp() throws Exception {
    a = new AllCitations();
  }


  @Test
  public void testExtract() {
    // Valid file and correct output 1
    String result = a.Extract("sample1.html");
    assertEquals("2. Number of All Citations: " + "\n" + "      437", result);
    // Valid file and correct output 2
    result = a.Extract("sample2.html");
    assertEquals("2. Number of All Citations: " + "\n" + "      263", result);
    // File Does not exist
    result = a.Extract("FileDoesn'tExist.html");
    assertEquals("malformed URL or cannot open connection to given URL",
        result);
    // Valid file and incorrect output 1
    result = a.Extract("sample1.html");
    assertNotEquals("this is clearly wrong", result);
    // Valid file and incorrect output 2
    result = a.Extract("sample2.html");
    assertNotEquals("i bet this is wrong", result);
  }

}
