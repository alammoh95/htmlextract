package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import driver.PaperCitation;

public class PaperCitationTest {
  PaperCitation a;

  @Before
  public void setUp() throws Exception {
    a = new PaperCitation();
  }

  @Test
  public void testExtract() {
    // Valid file and correct output 1
    String result = a.Extract("sample1.html");
    assertEquals("5. Total paper citation (first 5 papers):" + "\n"
        + "          239", result);
    // Valid file and correct output 2
    result = a.Extract("sample2.html");
    assertEquals("5. Total paper citation (first 5 papers):" + "\n"
        + "          158", result);
    // File Does not exist
    result = a.Extract("FileDoesn'tExist.html");
    assertEquals("malformed URL or cannot open connection to given URL",
        result);
    // Valid file and incorrect output 1
    result = a.Extract("sample1.html");
    assertNotEquals("this is clearly wrong", result);
    // Valid file and incorrect output 2
    result = a.Extract("sample2.html");
    assertNotEquals("i bet this is wrong", result);
  }

}
