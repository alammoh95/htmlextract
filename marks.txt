Your mark out of 10 is = 9.5/10.0
This assignment has been marked by TA: Raymond Lei

The email address of the TA is: yunyunsin@gmail.com

=====================================================

PART 1) +++SVN CHECKOUT ON YOUR ASSIGNMENT3+++
A    marks/alammoh5/sample1_files
A    marks/alammoh5/sample1_files/scholar_logo_md_2011.gif
A    marks/alammoh5/sample1_files/citations
A    marks/alammoh5/sample1_files/chart
A    marks/alammoh5/sample1.html
A    marks/alammoh5/sample2_files
A    marks/alammoh5/sample2_files/chart
A    marks/alammoh5/sample2_files/scholar_logo_md_2011.gif
A    marks/alammoh5/sample2_files/green_ghost.jpg
A    marks/alammoh5/sample2.html
A    marks/alammoh5/src
A    marks/alammoh5/src/driver
A    marks/alammoh5/src/driver/AllCitations.java
A    marks/alammoh5/src/driver/Extractor.java
A    marks/alammoh5/src/driver/TotalCoAuthors.java
A    marks/alammoh5/src/driver/Publications.java
A    marks/alammoh5/src/driver/CoAuthorListSorted.java
A    marks/alammoh5/src/driver/I10Index.java
A    marks/alammoh5/src/driver/Author.java
A    marks/alammoh5/src/driver/MyParser.java
A    marks/alammoh5/src/driver/PaperCitation.java
A    marks/alammoh5/src/driver/HTML.java
A    marks/alammoh5/src/test
A    marks/alammoh5/src/test/TotalCoAuthorsTest.java
A    marks/alammoh5/src/test/PublicationsTest.java
A    marks/alammoh5/src/test/CoAuthorListSortedTest.java
A    marks/alammoh5/src/test/I10IndexTest.java
A    marks/alammoh5/src/test/AuthorTest.java
A    marks/alammoh5/src/test/PaperCitationTest.java
A    marks/alammoh5/src/test/AllCitationsTest.java
Checked out revision 69.




PART 2) +++SVN COMMIT LOGS ON src+++
------------------------------------------------------------------------
r69 | alammoh5@UTORONTO.CA | 2014-11-29 23:43:11 -0500 (Sat, 29 Nov 2014) | 1 line


------------------------------------------------------------------------
r68 | alammoh5@UTORONTO.CA | 2014-11-29 23:36:13 -0500 (Sat, 29 Nov 2014) | 1 line

update javadocs and commenting
------------------------------------------------------------------------
r67 | alammoh5@UTORONTO.CA | 2014-11-29 23:33:53 -0500 (Sat, 29 Nov 2014) | 1 line

updated source formatting
------------------------------------------------------------------------
r66 | alammoh5@UTORONTO.CA | 2014-11-29 23:32:46 -0500 (Sat, 29 Nov 2014) | 1 line

updated formatting
------------------------------------------------------------------------
r65 | alammoh5@UTORONTO.CA | 2014-11-29 23:32:08 -0500 (Sat, 29 Nov 2014) | 1 line

completed test cases
------------------------------------------------------------------------
r64 | alammoh5@UTORONTO.CA | 2014-11-29 18:37:55 -0500 (Sat, 29 Nov 2014) | 1 line

formatting
------------------------------------------------------------------------
r63 | alammoh5@UTORONTO.CA | 2014-11-29 18:37:36 -0500 (Sat, 29 Nov 2014) | 1 line

formatting
------------------------------------------------------------------------
r62 | alammoh5@UTORONTO.CA | 2014-11-29 18:33:54 -0500 (Sat, 29 Nov 2014) | 1 line

Update comments
------------------------------------------------------------------------
r61 | alammoh5@UTORONTO.CA | 2014-11-29 15:02:06 -0500 (Sat, 29 Nov 2014) | 1 line

completed author test cases
------------------------------------------------------------------------
r60 | alammoh5@UTORONTO.CA | 2014-11-29 12:45:55 -0500 (Sat, 29 Nov 2014) | 1 line

AllCitations tests complete
------------------------------------------------------------------------
r59 | alammoh5@UTORONTO.CA | 2014-11-29 00:23:29 -0500 (Sat, 29 Nov 2014) | 1 line

Updated exception output to return statements
------------------------------------------------------------------------
r58 | alammoh5@UTORONTO.CA | 2014-11-27 13:32:11 -0500 (Thu, 27 Nov 2014) | 1 line

created test cases
------------------------------------------------------------------------
r57 | alammoh5@UTORONTO.CA | 2014-11-27 13:07:28 -0500 (Thu, 27 Nov 2014) | 1 line


------------------------------------------------------------------------
r56 | alammoh5@UTORONTO.CA | 2014-11-27 03:03:44 -0500 (Thu, 27 Nov 2014) | 1 line


------------------------------------------------------------------------
r55 | alammoh5@UTORONTO.CA | 2014-11-27 03:03:33 -0500 (Thu, 27 Nov 2014) | 1 line


------------------------------------------------------------------------
r54 | alammoh5@UTORONTO.CA | 2014-11-27 03:03:28 -0500 (Thu, 27 Nov 2014) | 1 line


------------------------------------------------------------------------
r53 | alammoh5@UTORONTO.CA | 2014-11-27 02:39:59 -0500 (Thu, 27 Nov 2014) | 1 line


------------------------------------------------------------------------
r52 | alammoh5@UTORONTO.CA | 2014-11-27 02:36:00 -0500 (Thu, 27 Nov 2014) | 1 line

finished co authors
------------------------------------------------------------------------
r51 | alammoh5@UTORONTO.CA | 2014-11-27 02:21:44 -0500 (Thu, 27 Nov 2014) | 2 lines

completed papercitations

------------------------------------------------------------------------
r50 | alammoh5@UTORONTO.CA | 2014-11-27 01:10:32 -0500 (Thu, 27 Nov 2014) | 1 line


------------------------------------------------------------------------
r49 | alammoh5@UTORONTO.CA | 2014-11-25 15:42:48 -0500 (Tue, 25 Nov 2014) | 1 line

completed publications
------------------------------------------------------------------------
r48 | alammoh5@UTORONTO.CA | 2014-11-25 15:20:04 -0500 (Tue, 25 Nov 2014) | 1 line

Completed publications
------------------------------------------------------------------------
r47 | alammoh5@UTORONTO.CA | 2014-11-25 14:58:13 -0500 (Tue, 25 Nov 2014) | 1 line

Completed i10Index
------------------------------------------------------------------------
r46 | alammoh5@UTORONTO.CA | 2014-11-25 14:33:42 -0500 (Tue, 25 Nov 2014) | 1 line

Completed all citations
------------------------------------------------------------------------
r45 | alammoh5@UTORONTO.CA | 2014-11-23 19:50:13 -0500 (Sun, 23 Nov 2014) | 1 line

remove unneeded constructor
------------------------------------------------------------------------
r44 | alammoh5@UTORONTO.CA | 2014-11-23 19:48:35 -0500 (Sun, 23 Nov 2014) | 2 lines

Create a nested loop to implement polymorphism while executing extract commands

------------------------------------------------------------------------
r43 | alammoh5@UTORONTO.CA | 2014-11-23 19:39:19 -0500 (Sun, 23 Nov 2014) | 1 line

Change constructors to take no arguments. input remains same for all commands
------------------------------------------------------------------------
r42 | alammoh5@UTORONTO.CA | 2014-11-23 19:16:28 -0500 (Sun, 23 Nov 2014) | 1 line

add unimplemented interface methods
------------------------------------------------------------------------
r41 | alammoh5@UTORONTO.CA | 2014-11-23 19:14:14 -0500 (Sun, 23 Nov 2014) | 1 line

rather than using inheritance, going to use an interface
------------------------------------------------------------------------
r40 | alammoh5@UTORONTO.CA | 2014-11-23 19:00:17 -0500 (Sun, 23 Nov 2014) | 1 line

create a HTML class for OOP
------------------------------------------------------------------------
r39 | alammoh5@UTORONTO.CA | 2014-11-23 18:33:02 -0500 (Sun, 23 Nov 2014) | 1 line

Implement SRP design
------------------------------------------------------------------------
r38 | alammoh5@UTORONTO.CA | 2014-11-23 18:12:38 -0500 (Sun, 23 Nov 2014) | 1 line

Honor Code
------------------------------------------------------------------------
r33 | attarwal@UTORONTO.CA | 2014-11-20 17:29:23 -0500 (Thu, 20 Nov 2014) | 1 line

A3 Starter code by instructor
------------------------------------------------------------------------




PART 3) +++SVN COMMIT LOGS ON test+++
------------------------------------------------------------------------
r69 | alammoh5@UTORONTO.CA | 2014-11-29 23:43:11 -0500 (Sat, 29 Nov 2014) | 1 line


------------------------------------------------------------------------
r67 | alammoh5@UTORONTO.CA | 2014-11-29 23:33:53 -0500 (Sat, 29 Nov 2014) | 1 line

updated source formatting
------------------------------------------------------------------------
r65 | alammoh5@UTORONTO.CA | 2014-11-29 23:32:08 -0500 (Sat, 29 Nov 2014) | 1 line

completed test cases
------------------------------------------------------------------------
r64 | alammoh5@UTORONTO.CA | 2014-11-29 18:37:55 -0500 (Sat, 29 Nov 2014) | 1 line

formatting
------------------------------------------------------------------------
r61 | alammoh5@UTORONTO.CA | 2014-11-29 15:02:06 -0500 (Sat, 29 Nov 2014) | 1 line

completed author test cases
------------------------------------------------------------------------
r60 | alammoh5@UTORONTO.CA | 2014-11-29 12:45:55 -0500 (Sat, 29 Nov 2014) | 1 line

AllCitations tests complete
------------------------------------------------------------------------
r58 | alammoh5@UTORONTO.CA | 2014-11-27 13:32:11 -0500 (Thu, 27 Nov 2014) | 1 line

created test cases
------------------------------------------------------------------------
r33 | attarwal@UTORONTO.CA | 2014-11-20 17:29:23 -0500 (Thu, 20 Nov 2014) | 1 line

A3 Starter code by instructor
------------------------------------------------------------------------




PART 4) +++HONOR CODE?+++
// **********************************************************
// Assignment3:
// UTORID user_name: alammoh5
//
// Author: Mohammad Osama Alam
//
//
// Honor Code: I pledge that this program represents my own
// program code and that I have coded on my own. I received
// help from no one in designing and debugging my program.
// *********************************************************




PART 5) +++FOLLOWING LINES ARE GREATER THAN 80 CHARACTERS+++
./marks/alammoh5/src/driver/CoAuthorListSorted.java:2: * @author Osama This class extracts the names of the co-authors and orders them
./marks/alammoh5/src/driver/CoAuthorListSorted.java:29:            "<a class=\"cit-dark-link\" href=(.*?)" + " title=(.*?)>(.*?)</a>";
./marks/alammoh5/src/driver/Author.java:29:            ("1. Name of Author: " + "\n" + "      " + matcherObject.group(1));

MARK DEDUCT 0.5




PART 6) +++IDE FILES+++
Good, you have no IDE files in your SVN repository




Part 7) +++ COMPILING YOUR ASSIGNMENT +++
Buildfile: /home/yunyunzai/Dropbox/dgp/CSC207TA2014_FALL/A3/marks/alammoh5/build.xml

compile:
    [mkdir] Created dir: /home/yunyunzai/Dropbox/dgp/CSC207TA2014_FALL/A3/marks/alammoh5/bin
    [javac] /home/yunyunzai/Dropbox/dgp/CSC207TA2014_FALL/A3/marks/alammoh5/build.xml:8: warning: 'includeantruntime' was not set, defaulting to build.sysclasspath=last; set to false for repeatable builds
    [javac] Compiling 17 source files to /home/yunyunzai/Dropbox/dgp/CSC207TA2014_FALL/A3/marks/alammoh5/bin
    [javac] Note: Some input files use unchecked or unsafe operations.
    [javac] Note: Recompile with -Xlint:unchecked for details.

BUILD SUCCESSFUL
Total time: 1 second

Good, program built successfully.




Part 8) +++RUNNING TEST CASE1 WITH NO OUTPUT FILE+++ with dj.html
----------------------------------------------------------------
1. Name of Author: 
      Deepak Jagdish
2. Number of All Citations: 
      23
3.  Number of i10-index after 2009: 
      0
4. Title of the first 3 publications: 
      1- Nokia internet pulse: a long term deployment and iteration of a twitter visualization
      2- Real time collaborative video annotation using Google App Engine and XMPP protocol
      3- Sonic Grid: an auditory interface for the visually impaired to navigate GUI-based environments
5. Total paper citation (first 5 papers):
          23
6. Total Co-Authors:
          5
----------------------------------------------------------------
7. Co-Author list sorted (Total: 5)
Abbas Attarwala
Blair MacIntyre
Joseph 'Jofish' Kaye
Yan Xu
rahul sawhney




Part 9) +++RUNNING TEST CASE2 WITH NO OUTPUT FILE+++ with dj.html and rs.html
----------------------------------------------------------------
1. Name of Author: 
      Deepak Jagdish
2. Number of All Citations: 
      23
3.  Number of i10-index after 2009: 
      0
4. Title of the first 3 publications: 
      1- Nokia internet pulse: a long term deployment and iteration of a twitter visualization
      2- Real time collaborative video annotation using Google App Engine and XMPP protocol
      3- Sonic Grid: an auditory interface for the visually impaired to navigate GUI-based environments
5. Total paper citation (first 5 papers):
          23
6. Total Co-Authors:
          5
----------------------------------------------------------------
1. Name of Author: 
      rahul sawhney
2. Number of All Citations: 
      33
3.  Number of i10-index after 2009: 
      1
4. Title of the first 3 publications: 
      1- On fast exploration in 2D and 3D terrains with multiple robots
      2- Sonic Grid: an auditory interface for the visually impaired to navigate GUI-based environments
      3- Multi robotic exploration with communication requirement to a fixed base station
5. Total paper citation (first 5 papers):
          33
6. Total Co-Authors:
          0
----------------------------------------------------------------
7. Co-Author list sorted (Total: 5)
Abbas Attarwala
Blair MacIntyre
Joseph 'Jofish' Kaye
Yan Xu
rahul sawhney




Part 10) +++RUNNING TEST CASE2 WITH OUTPUT FILE+++ with dj.html and rs.html





Part 11) +++SRP CHECK+++
SRP followed. Good!




